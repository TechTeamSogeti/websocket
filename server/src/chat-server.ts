import * as http from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';

import { Message } from './model/message';

export class ChatServer {
    public static readonly PORT:number = 8080;
    private app: express.Application;
    private server: http.Server;
    private io: SocketIO.Server;
    private port: string | number;

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }

    private createApp(): void {
        this.app = express();
    }

    private createServer(): void {
        this.server = http.createServer(this.app);
    }

    private config(): void {
        this.port = process.env.PORT || ChatServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });

        this.io.on('connect', (socket: any) => {
            socket.on('message', (m: Message) => {
                this.io.emit('message', m);
            });
            socket.on('disconnect', () => {});
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}
