export enum Event {
    CONNECT = 'connect',
    LEFT = 'left',
    DISCONNECT = 'disconnect'
}
